<!DOCTYPE html>
<html>
<head>
<title>Chat-app</title>

<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- jQuery library -->
<!-- Latest compiled and minified CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Latest compiled JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
</head>
<body>

    <div class="container col-6 offset-3">
        <h1 class="text-center">Chat Here</h1>


        <div class="show">
            @foreach($chats as $chat)
            <h4><b>{{$chat->message}}</b></h4>
            <p>{{Str::limit($chat->name , 5) }} {{ $chat->created_at->diffForHumans() }}</p>
           
            @endforeach

        </div>


            <form id="chat">
                @csrf
                <div class="">
                    {{-- <label class="form-label">comment</label> --}}
                    {{-- <p>nice</p> --}}
                    <input type="hidden" id="name" name="name" value="{{ Auth::user()->name }}" class="form-control">
                    <textarea class="form-control" id="message" name="message" cols="20" rows="3" placeholder="message"></textarea>
                    <button type="button" class="btn btn-success send">send</button>
                </div>
            </form>
        

    </div>

    <script>
        $(document).ready(function(){
           $(document).on('click','.send',function(e){
             e.preventDefault();
            let name=$('#name').val();
            let message=$('#message').val();
            console.log(name+message);

             $.ajax({

                url: "{{route('chat.store')}}",
                method: 'post',
                data:{name:name,message:message},
                success:function(response){

                    $('.show').load(location.href+' .show');

                },error:function(err){
                    let error = err.responseJSON;
                    $.each(error.errors,function(index, value){

                    })
                }



             })
           
           });
        });



    </script>

    <script>
        $.ajaxSetup({
         headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
        });
    </script>

</body>
</html>






