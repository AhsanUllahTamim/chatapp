<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    //

    public function index(){


        return view('chat');

    }
    public function show(){

        $chats = Chat::orderBy('id')->get();

        // return response()->json(['chats'=>$chats]);
        //  dd($chats);
         return view('chat', compact('chats'));

    }

    public function store(Request $request){

        $chat =new Chat();
        $chat->name = $request->name;
        $chat->message = $request->message;
        $chat->save();

    }
   

   
}
